$(function() {
    var acr = $('.accordion');
    var i;
    for (i = 0; i < acr.length; i++) {
        $('.accordion').eq(i).click(function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }

    $('.move-down').click(function() {
        var self = $(this);
        item = self.parents('div.element');
        swap = item.next();
        item.before(swap.detach());
    });
    $('.move-up').click(function() {
        var self = $(this);
        item = self.parents('div.element');
        swap = item.prev();
        item.after(swap.detach());
    });
});
