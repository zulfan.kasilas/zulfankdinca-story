from http import HTTPStatus
from django.test import TestCase, Client, RequestFactory
from django.urls import resolve, reverse
from .apps import Story8Config
from .views import index, loginView, logoutView, registerView
from . import apps
from django.contrib.auth.models import User, AnonymousUser

# Create your tests here.

class Tests(TestCase):
    def test_url_resolved(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
    def test_url_resolved2(self):
        response = Client().get('/story8/data/?q=')
        self.assertEquals(response.status_code, 200)

class TestApp(TestCase):
    def testConfig(self):
        self.assertEqual(Story8Config.name, 'Story8')

class TestView(TestCase):
	def test_url_login_resolved(self):
		response = Client().get('/story8/login/')
		self.assertEquals(response.status_code, 200)

	def test_login_GET_can_login(self):
		new_account = User.objects.create_user('zylfan', 'zylfan@gmail.com', 'zulzul')
		new_account.set_password('zulzul')
		new_account.save()

		c = Client()
		login= c.login(username=new_account.username, password='zulzul')

		response=c.get('/story8/login/')
		self.assertEqual(response['location'],'/story8/')

	def test_login_POST_can_login(self):
		new_account = User.objects.create_user('zylfan', 'zylfan@gmail.com', 'zulzul')
		new_account.set_password('zulzul')
		new_account.save()

		c = Client()

		response=c.post(
			'/story8/login/',
			data = { 
			'username' : 'zylfan',
			'password' : 'zulzul', 
			})
		self.assertEqual(response['location'],'/story8/')

	def test_login_POST_cant_login(self):
		c = Client()

		response=c.post(
			'/story8/login/',
			data = { 
			'username' : 'zylfan',
			'password' : 'zulzul', 
			})
		self.assertEqual(response['location'],'/story8/login/')

	def test_url_logout_resolved(self):
		response = Client().get('/story8/logout/')
		self.assertEquals(response.status_code, 302)

	def test_logout_GET_can_logout(self):
		new_account = User.objects.create_user('zylfan', 'zylfan@gmail.com', 'zulzul')
		new_account.set_password('zulzul')
		new_account.save()

		c = Client()
		login= c.login(username=new_account.username, password='zulzul')

		response=c.get('/story8/logout/')
		self.assertEqual(response['location'],'/story8/')

	def test_url_register_resolved(self):
		response = Client().get('/story8/register/')
		self.assertEquals(response.status_code, 200)

	def test_register_POST_can_register(self):
		c= Client()
		response=c.post(
			'/story8/register/',
			data={
				'user_name_register' : 'zylfan',
				'user_email_register' : 'zylfan@gmail.com',
				'user_password_register' : 'zulzul',
			}
		)
		self.assertEqual(response['location'],'/story8/')

	def test_login_GET_already_register(self):
		new_account = User.objects.create_user('zylfan', 'zylfan@gmail.com', 'zulzul')
		new_account.set_password('zulzul')
		new_account.save()

		c = Client()
		login= c.login(username=new_account.username, password='zulzul')

		response=c.get('/story8/register/')
		self.assertEqual(response['location'],'/story8/')

	def test_register_POST_cannot_register_same_username(self):
		new_account = User.objects.create_user('zylfan', 'zylfan@gmail.com', 'zulzul')
		new_account.set_password('zulfzul')
		new_account.save()

		c = Client()
		response=c.post(
			'/story8/register/',
			data={
				'user_name_register' : 'zylfan',
				'user_email_register' : 'zylfano@gmail.com',
				'user_password_register' : 'zulzul',
			}
		)
		self.assertTemplateUsed(response,'register.html')
		html_response = response.content.decode('utf8')
		self.assertIn('Maaf username sudah ada', html_response)

	def test_register_POST_cannot_register_same_email(self):
		new_account = User.objects.create_user('zylfan', 'zylfan@gmail.com', 'zulzul')
		new_account.set_password('zulzul')
		new_account.save()

		c= Client()
		response=c.post(
			'/story8/register/',
			data={
				'user_name_register' : 'zulo',
				'user_email_register' : 'zylfan@gmail.com',
				'user_password_register' : 'zulzul',
				}
		)
		self.assertTemplateUsed(response,'register.html')
		html_response = response.content.decode('utf8')
		self.assertIn('Maaf email sudah ada', html_response)

	def test_register_POST_cannot_register_same_email_and_username(self):
		new_account = User.objects.create_user('zylfan', 'zylfan@gmail.com', 'zylfan')
		new_account.set_password('zylfan')
		new_account.save()

		c= Client()
		response=c.post(
			'/story8/register/',
			data={
				'user_name_register' : 'zylfan',
				'user_email_register' : 'zylfan@gmail.com',
				'user_password_register' : 'zylfan',
			}
		)
		self.assertTemplateUsed(response,'register.html')
		html_response = response.content.decode('utf8')
		self.assertIn('Maaf username dan email sudah ada', html_response)