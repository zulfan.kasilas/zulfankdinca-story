from django.urls import path
from django.contrib import admin

from . import views

app_name = 'Story8'

urlpatterns = [
	path('admin/', admin.site.urls),
	path('', views.index, name='index'),
	path('login/', views.loginView, name='login'),
	path('logout/', views.logoutView, name='logout'),
	path('register/', views.registerView, name='register'),
	path('data/', views.fungsi_data),
]
