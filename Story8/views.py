from django.shortcuts import render, redirect, resolve_url
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


import requests
import json

# Create your views here.
def index(request):
	response = {}

	return render(request, 'story8.html', response)

def loginView(request):
	response = {}
	if (not request.user.is_anonymous):
		return redirect('/story8/')

	if request.method == "POST":
		username_login = request.POST.get('username',False)
		password_login = request.POST.get('password',False)

		user = authenticate(request, username = username_login, password = password_login)

		if user is not None:
			login(request, user)
			return redirect('/story8/')
		else :
			return redirect('/story8/login/')

	return render(request, 'login.html', response)

def logoutView(request):
	logout(request)
	return redirect('/story8/')

def registerView(request):
	response = {
		'error' : False,
		'isiError' : '',
		'errorUsername' : '',
	}

	if (not request.user.is_anonymous):
		return redirect('/story8/')

	response['error']=False

	if request.method == "POST":
		name = request.POST.get('user_name_register',False)
		email = request.POST.get('user_email_register',False)
		password = request.POST.get('user_password_register',False)

		try:
			account = User.objects.get(username = name)
		except:
			account = None

		try:
			accountEmail = User.objects.get(email= email)
		except:
			accountEmail = None

		if (account == None and accountEmail == None):
			new_account = User.objects.create_user(name,email,password)
			new_account.save()
			login(request, new_account)
			return redirect('/story8/')

		elif (account!= None and accountEmail == None):
			response['error'] = True
			response['isiError'] = 'Maaf username sudah ada'
			return render(request,'register.html',response)

		elif (account== None and accountEmail != None):
			response['error'] = True
			response['isiError'] = 'Maaf email sudah ada'
			return render(request,'register.html',response)

		else:
			response['errorUsername']=True
			response['error']=True
			response['isiError']='Maaf username dan email sudah ada'
			return render(request,'register.html',response)

	return render(request,'register.html',response)


def fungsi_data(request):
	url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
	ret = requests.get(url)
	data = json.loads(ret.content)
	return JsonResponse(data, safe=False)