from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('story1', views.story1, name='story1'),
    path('experience', views.experience, name='experience'),

]
