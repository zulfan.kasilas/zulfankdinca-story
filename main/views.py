from django.shortcuts import render

def home(request):
    return render(request, 'main/home.html')
    
def experience(request):
	return render(request, 'main/experience.html')

def story1(request):
	return render(request, 'main/story1.html')


