from django.db import models
from django import forms

# Create your models here.
class Jadwal(models.Model):
	Nama = models.TextField(max_length=50)
	Dosen = models.TextField(max_length=50)
	SKS = models.TextField(max_length=50)
	Waktu = models.TimeField()
	Ruang = models.TextField(max_length=50)
	Deskripsi = models.TextField(max_length=50)