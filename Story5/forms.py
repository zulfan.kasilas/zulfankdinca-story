from django import forms

class FormJadwal(forms.Form):

	Nama = forms.CharField(widget=forms.TextInput(attrs={
		'class' : 'form-control',
		'type' : 'text',
		'placeholder' : 'Nama Mata Kuliah',
		'required' : True,
	}))

	Dosen = forms.CharField(widget=forms.TextInput(attrs={
		'class' : 'form-control',
		'type' : 'text',
		'placeholder' : 'Nama Dosen',
		'required' : True,
	}))

	SKS = forms.CharField(widget=forms.TextInput(attrs={
		'class' : 'form-control',
		'type' : 'text',
		'placeholder' : 'Jumlah SKS',
		'required' : True,
	}))

	Waktu = forms.TimeField(widget=forms.TimeInput(attrs={
		'class' : 'form-control',
		'type' : 'time',
		'required' : True,
	}))

	Ruang = forms.CharField(widget=forms.TextInput(attrs={
		'class' : 'form-control',
		'type' : 'text',
		'placeholder' : 'Ruang',
		'required' : True,
	}))

	Deskripsi = forms.CharField(widget=forms.TextInput(attrs={
		'class' : 'form-control',
		'type' : 'text',
		'placeholder' : 'Penjelasan',
		'required' : True,
	}))