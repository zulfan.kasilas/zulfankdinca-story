from django.urls import path

from . import views

app_name = 'Story5'

urlpatterns = [
	path('', views.Join, name='jadwal'),
    path('jadwal', views.Join, name='jadwal'),
    path('detail/<int:pk>', views.detail, name='detail'),
    path('<int:pk>',views.sched_delete,name = 'Delete'),

]
