from django.shortcuts import render, redirect
from .models import Jadwal as jadwal
from .forms import FormJadwal

# Create your views here.
def Join(request):
    if request.method == "POST":
        form = FormJadwal(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.Nama = form.cleaned_data['Nama']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.SKS = form.cleaned_data['SKS']
            sched.Waktu = form.cleaned_data['Waktu']
            sched.Ruang = form.cleaned_data['Ruang']
            sched.Deskripsi = form.cleaned_data['Deskripsi']
            sched.save()
        return redirect('/story5/jadwal')
    else:
        sched = jadwal.objects.all()
        form = FormJadwal()
        response = {"sched":sched, 'form' : form}
        return render(request,'jadwal.html',response)

def sched_delete(request, pk):
    if request.method == "POST":
        form = FormJadwal(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.Nama = form.cleaned_data['Nama']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.SKS = form.cleaned_data['SKS']
            sched.Waktu = form.cleaned_data['Waktu']
            sched.Ruang = form.cleaned_data['Ruang']
            sched.Deskripsi = form.cleaned_data['Deskripsi']
            sched.save()
        return redirect('/story5/jadwal')
    else:
        jadwal.objects.filter(pk=pk).delete()
        data = jadwal.objects.all()
        form = FormJadwal()
        response = {"sched":data, 'form' : form}
        return render(request, 'jadwal.html', response)

def detail(request, pk):
    sched = jadwal.objects.all().filter(id=pk)
    response = {"FormJadwal" : sched}
    return render(request, "detail.html", response)
