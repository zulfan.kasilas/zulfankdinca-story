from django.test import TestCase, Client
from django.urls import resolve

from .views import Index
from . import apps


# Create your tests here.

class Test(TestCase):

    # URL Testcases
    def test_url_story7(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    # View Testcases
    def test_view_function_index(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, Index)

    # Template Testcases
    def test_template_story7(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'anything.html')

    # Apps
    def test_app(self):
    	appname = apps.Story7Config.name
    	self.assertEqual(appname,'Story7')