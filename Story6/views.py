from django.shortcuts import render, redirect
from .models import Kegiatan,Peserta
from .forms import formKegiatan, formPeserta

# Create your views here.
def list_kegiatan(request):
    list_kegiatan = Kegiatan.objects.all()
    data_kegiatan = []
    for kegiatan in list_kegiatan:
        list_peserta = Peserta.objects.filter(kegiatan=kegiatan)
        data_peserta = []
        for peserta in list_peserta:
            data_peserta.append(peserta)
        data_kegiatan.append((kegiatan,data_peserta))
    context = {'data_kegiatan':data_kegiatan,'number':5}
    return render(request,'kegiatan.html',context)

def tambah_peserta(request):
    form = formPeserta()
    if (form.is_valid and request.method == 'POST'):
        form = formPeserta(request.POST)
        form.save()
        return redirect('/story6')
    context = {'form':form,'number':5}
    return render(request,'tambahpeserta.html',context)

def tambah_kegiatan(request):
    form = formKegiatan()
    if (form.is_valid and request.method == 'POST'):
        form = formKegiatan(request.POST)
        form.save()
        return redirect('/story6')

    context = {'form':form,'number':5}
    
    return render(request,'tambahkegiatan.html',context)
