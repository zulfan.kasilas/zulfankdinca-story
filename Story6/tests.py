from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Kegiatan,Peserta
from . import views
from .apps import Story6Config

# Create your tests here.
class Testing123(TestCase):
	
	def test_url_kegiatan(self):
		response = Client().get('/story6/')
		self.assertEquals(response.status_code,200)

	def test_url_tambahpeserta(self):
		response = Client().get('/story6/tambahpeserta')
		self.assertEquals(response.status_code,200)

	def test_url_tambahkegiatan(self):
		response = Client().get('/story6/tambahkegiatan')
		self.assertEquals(response.status_code,200)

	def test_cek_button_tambahpeserta(self):
		response = Client().get('/story6/')
		html = response.content.decode('utf-8')
		self.assertIn('Tambah Peserta', html)

	def test_cek_button_tambahkegiatan(self):
		response = Client().get('/story6/')
		html = response.content.decode('utf-8')
		self.assertIn('Tambah Kegiatan', html)

	def test_cek_button_tambah_di_tambahkegitan(self):
		response = Client().get('/story6/tambahkegiatan')
		html = response.content.decode('utf-8')
		self.assertIn('Tambah', html)

	def test_cek_button_batal_di_tambahkegitan(self):
		response = Client().get('/story6/tambahkegiatan')
		html = response.content.decode('utf-8')
		self.assertIn('Batal', html)

	def test_cek_button_tambah_di_tambahpeserta(self):
		response = Client().get('/story6/tambahpeserta')
		html = response.content.decode('utf-8')
		self.assertIn('Tambah', html)
	
	def testConfig(self):
		self.assertEqual(Story6Config.name, 'Story6')

class ModelTest(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="Belajar Bareng")
        self.peserta = Peserta.objects.create(nama_peserta="Zulfan")

    def test_model_dibuat(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(Peserta.objects.count(), 1)

    def test_str(self):
        self.assertEquals(str(self.kegiatan), "Belajar Bareng")
        self.assertEquals(str(self.peserta), "Zulfan")
