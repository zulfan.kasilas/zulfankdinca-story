from django import forms
from .models import Kegiatan, Peserta

class formKegiatan(forms.ModelForm):
	class Meta:
		model = Kegiatan
		fields = "__all__"

class formPeserta(forms.ModelForm):
	class Meta:
		model = Peserta
		fields = "__all__"
	
