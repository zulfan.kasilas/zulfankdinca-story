from django.urls import path

from . import views

app_name = 'Story6'

urlpatterns = [
	path('', views.list_kegiatan, name='kegiatan'),
    path('tambahpeserta', views.tambah_peserta, name='tambahpeserta'),
    path('tambahkegiatan', views.tambah_kegiatan, name='tambahkegiatan'),
]
