from django.db import models

# Create your models here.
class Kegiatan(models.Model):
	nama_kegiatan = models.CharField(max_length=50)
	def __str__(self):
		return self.nama_kegiatan

class Peserta(models.Model):
	nama_peserta = models.CharField(max_length=50)
	kegiatan = models.ForeignKey(Kegiatan, null = True, on_delete = models.SET_NULL)

	def __str__(self):
		return self.nama_peserta;
	